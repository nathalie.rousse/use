# Use

Project containing some **notes and devs** as **Software User**.

The 'main' branch contains :

  - Hugo site : **https://nathalie.rousse.pages.mia.inra.fr/use**
  *([dev notes for Hugo Site](./hugo-site.md))*

  - **devs** folder :
    **[Software Heritage](./devs/SWH/README.md)**,
    [REDELAC](./devs/REDELAC/README.md) ...

The other branches are created by **uses** : about documentation, use cases,
development...


## [Siwaa API](/../siwaa-api/README.md)

**'siwaa-api' branch** : Siwaa API.

See [README.md](/../siwaa-api/README.md) of 'siwaa-api' branch.

## [GAMA](/../gama/README.md)

**'gama' branch** : GAMA platform (use of containers, Galaxy tools, Siwaa API).

See [README.md](/../gama/README.md) of 'gama' branch.

See some presentations : 
'[Case of deployed Galaxy tools](https://view.genially.com/67361387d808f58a421f9e44/interactive-content-gama-desc-gen-run-tools)',
*'[A case in development state](https://view.genially.com/6654303dac73ac00151ced16/interactive-content-gamatool)'*.

## [A Dense Neural Network](/../dnn/README.md)

**'dnn' branch** : A Dense Neural Network (use of containers, Galaxy tools,
Siwaa API).

See [README.md](/../dnn/README.md) of 'dnn' branch.

## [Data INRAE data.inrae.fr](/../dataverse/README.md)

**'dataverse' branch** : Data INRAE data.inrae.fr.

See [README.md](/../dataverse/README.md) of 'dataverse' branch.


## [NextCloud INRAE https://nextcloud.inrae.fr](/../nextcloud/README.md)

**'nextcloud' branch** : NextCloud INRAE https://nextcloud.inrae.fr.

See [README.md](/../nextcloud/README.md) of 'nextcloud' branch.


## [An environment to run Jupyter Notebooks](/../ipynb-env/README.md)

**'ipynb-env' branch** : an environment to run Jupyter Notebooks

See [README.md](/../ipynb-env/README.md) of 'ipynb-env' branch.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fnathalie.rousse%2Fuse/ipynb-env)
environment to run Jupyter Notebooks

