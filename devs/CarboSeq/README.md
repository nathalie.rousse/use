# CarboSeq

This development has led to **development of a R package** into **project 
'[CarboSeq SIWAA API](https://forgemia.inra.fr/record/carboseqsiwaa)'**.
This development content has mainly been included into the R package,
some minor complements to the R Package remaining here.

# Desc

*06/06/24*

- **https://forgemia.inra.fr/carboseq/record-projet-carboseq/-/issues/26** :
  
  - Depuis le jupyter lab de Carboseq (https://coby.infosol.inrae.fr:8080),
    piloter (langage R) le tool de Siwaa 'CarboSeqSimulator' pour execution
    des simulations sur meso@lr.

  - Galaxy tool :
    - tool: id="CarboSeqSimulator" | name="CarboSeqSimulator" | version="1.0.0"
    - input: param name="input" | type="data" | format="zip" |
      label="CarboSeq input (zip)"
    - output: data format='zip' | name="output" | label="CarboSeq output (zip)"

  - Input example : [40Sites.zip](./INPUTS/40Sites/40Sites.zip)

  - Sur la forge :
    - Tool: Projet carboseq/record-projet-carboseqp -> Branch carboseqnewsim 
      - https://forgemia.inra.fr/carboseq/record-projet-carboseq/-/tree/carboseqnewsim
      - .xml : https://forgemia.inra.fr/carboseq/record-projet-carboseq/-/tree/carboseqnewsim/tools/CarboSeqSimulator
    - Container :
      https://forgemia.inra.fr/carboseq/csopralibs_dev/-/tree/develop/Dockerfiles

- **Siwaa** (instance of Galaxy) :
  - Siwaa web site : https://siwaa.pages.mia.inra.fr/com.
  - There are different ways how to access to Siwaa tools:
    - through Siwaa **Web interface** : https://siwaa.toulouse.inrae.fr.
    - through [Siwaa **API**](/../siwaa-api/README.md).
    - for more, see [Siwaa Access](/../siwaa-api/access/README.md).
  - **Some help** :
    - Request for list of histories :
      https://siwaa.toulouse.inrae.fr/api/histories
    - Request for list of tools :
      https://siwaa.toulouse.inrae.fr/api/tools
    - **How to get an available API KEY value** : see
      '[Token](/../siwaa-api/token/README.md)'.

*28/06/24*

- See now **project 
  '[CarboSeq SIWAA API](https://forgemia.inra.fr/record/carboseqsiwaa)'**.

# R code preparation

- **R code has now been written as R package** into **project 
  '[CarboSeq SIWAA API](https://forgemia.inra.fr/record/carboseqsiwaa)'**.

- **Latest R code** before R Package ([003](./003) folder) :

  - **[SiwaaCarboSeqSimulator.R](./003/SiwaaCarboSeqSimulator.R)** :
    methods to command Siwaa tool 'CarboSeqSimulator'.

  - **[USING SiwaaCarboSeqSimulator.R](./003/call.R)** :
    code example of using SiwaaCarboSeqSimulator.R (run tool
    'CarboSeqSimulator', wait, download results).

- Previous R code ([002](./002) folder) :

  - **[CarboSeqSimulator_tool.R](./002/CarboSeqSimulator_tool.R)** :
    methods to command Siwaa tool 'CarboSeqSimulator'.

  - **[USING CarboSeqSimulator_tool.R](./002/README.md)** :
    example of a sequence using CarboSeqSimulator_tool.R (run tool
    'CarboSeqSimulator', wait, download results).

  - To use CarboSeqSimulator_tool.R methods into R code :
    ```R
    source("CarboSeqSimulator_tool.R") # path to CarboSeqSimulator_tool.R
    ```

