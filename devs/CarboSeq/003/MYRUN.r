
# profiling
Rprof(file.path(fs::path_wd(), "profiling_rprof.out"))
Rprofmem(file.path(fs::path_wd(), "profiling_mem.out"))

sysEnvConfig <- Sys.getenv(c(
                            "CSOPRALIBS_PARAM_OUTPUT",
                            "CARBOSEQPREPROCESSOR_INPUT_DIR"
                            ))


# full path to the existing directory where to client csv files are located
srcDir <- sysEnvConfig[["CARBOSEQPREPROCESSOR_INPUT_DIR"]]
srcDir <- sysEnvConfig[["CARBOSEQPREPROCESSOR_INPUT_DIR"]]

# output directory if writeResultsToOutput is TRUE
output <- sysEnvConfig[["CSOPRALIBS_PARAM_OUTPUT"]]


# Our call
modeltoolbox::loadCSVs(csvsDir = srcDir)

modeltoolbox::runSims(maxCores = 0)

modeltoolbox::saveSocForcingsResults(destiDir = output)


# Analyse profiling
summaryRprof(file.path(fs::path_wd(), "profiling_rprof.out"))
#summaryRprofmem(file.path(fs::path_home(), "profiling_mem.out"))

############################################
Patrick ticket :

> source(CarboSeqBatch.R)
> API_KEY = '9e38ceeedc3c14a215bba53273ff1384'
> jobinfo <- runSimsBatch(csvsDir = srcDir) # csvsDir est une chaîne de caractère qui contient le chemin d'un répertoire contenant les inputs non zipé.
# Par défaut la commande crée un historique
# zip le contenu des entrées
# transfert les fichiers
# lance le job
# retourne quelque-chose qui permet de suivre le job
> waitSimsResult(jobinfo)
# qui boucle trace un peu & fait patienter
> getSimsResult(jobinfo)
# qui récupére le résultat
# dzip
# charge en mémoire.


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

#modeltoolbox::loadCSVs(csvsDir = srcDir)
uploadCSVs(csvsDir = srcDir)


#modeltoolbox::runSims(maxCores = 0)
runSims(...)


#modeltoolbox::saveSocForcingsResults(destiDir = output)
saveResults(destiDir = output)a # download_file



