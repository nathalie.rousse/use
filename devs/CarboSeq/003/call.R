# R
#-----------------------------------------------------------------
author__="Nathalie Rousse (nathalie.rousse@inrae.fr)"
copyright__="Copyright (C) 2024, Inrae (https://www.inrae.fr)"
license__="MIT"
#-----------------------------------------------------------------

###############################################################################
# 
#                  SiwaaCarboSeqSimulator.R Use Case 
# 
###############################################################################

source("SiwaaCarboSeqSimulator.R") # path to CarboSeqSimulator_tool.R

siwaaenv = createSiwaaEnv()
siwaaenv = setSiwaaEnv(env=siwaaenv, API_KEY='9e38ceeedc3c14a215bba53273ff1384')
siwaaenv = setSiwaaEnv(env=siwaaenv, OUTPUT="./output0")

input_data_info <- uploadCSVs(env=siwaaenv, 
                              csvs_dir_path="../INPUTS/40Sites/csvdir")

output_data_info <- runSims(env=siwaaenv, input_data_info=input_data_info)

ret_survey = waitSimsResults(env=siwaaenv, data_info=output_data_info)
cat("ret_survey$end: ", ret_survey$end, " ** ",
    "ret_survey$data_ready: ", ret_survey$data_ready, "\n")

# At this step :
# -  If end and not data_ready, then STOP.
# -  If not end, then you can call again 'wait_sim_results' to continue survey
# -  If end and data_ready, then you can download results.

output_data_info = ret_survey$data_info

# Download the output data file
output_file_path <- saveResults(env=siwaaenv, data_info=output_data_info)
if (output_file_path != "") cat("OK") else cat("NOT OK")

# Download the output data in memory
ret_data = downloadInMemory(env=siwaaenv, data_info=output_data_info)
if (ret_data$cr_ok) output_file_data = ret_data$value

clearHistory(env=siwaaenv, data_info=output_data_info)

