# Siwaa workflow invocation report

Some general URLs :
- Workflow :
  - https://siwaa.toulouse.inrae.fr/api/workflows
  - https://siwaa.toulouse.inrae.fr/api/workflows/28b1a3fca48be365
- Workflow invocation :
  - https://siwaa.toulouse.inrae.fr/api/invocations
  - https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721

# Some requests giving information about workflow invocation

**show_invocation** :
[show_invocation.json](./show_invocation.json)
  - /invocations/invocation_id :
  - example:
    https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721
  - Get a workflow invocation dictionary representing the **scheduling of a
    workflow**. *May be sparse at first (missing inputs and invocation steps) 
    and will become more populated as the workflow is actually scheduled.*

**show_invocation_step** :
[show_invocation_step.json](./show_invocation_step.json)
  - /invocations/invocation_id/steps/step_id :
  - example:
    https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721/steps/07081866f13f77b2
  - See the **details** of a particular workflow invocation **step**.

**Summaries** :
[get_invocation_summary.json](./get_invocation_summary.json) |
[get_invocation_step_jobs_summary.json](./get_invocation_step_jobs_summary.json)

  - **get_invocation_summary** :
    [get_invocation_summary.json](./get_invocation_summary.json)
    - /invocations/invocation_id/jobs_summary :
    - example:
      https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721/jobs_summary
    - Get a **summary of an invocation**, stating the **number of jobs** which
      **succeed**, which are **paused** and which have **errored**.

  - **get_invocation_step_jobs_summary** :
    [get_invocation_step_jobs_summary.json](./get_invocation_step_jobs_summary.json)
    - /invocations/invocation_id/step_jobs_summary :
    - example:
      https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721/step_jobs_summary
    - Get a **detailed summary of an invocation**, listing **all jobs** with
      their **job IDs** and **current states**.

**get_invocation_report** :
[get_invocation_report.json](./get_invocation_report.json)
  - /invocations/invocation_id/report :
  - example:
    https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721/report
  - Get a Markdown **report for an invocation**.

# R Code

```R
server = "https://siwaa.toulouse.inrae.fr"
workflow_id = '28b1a3fca48be365'
invocation_id = '9a7f1d7cd30e6721'
step_id = '07081866f13f77b2'

url = paste(server, "/api", "/invocations/", invocation_id, sep="")
cat("show_invocation: ", url, "\n")

url = paste(server, "/api", "/invocations/", invocation_id, "/steps/", step_id, sep="")
cat("show_invocation_step: ", url, "\n")

url = paste(server, "/api", "/invocations/", invocation_id, "/jobs_summary", sep="")
cat("get_invocation_summary: ", url, "\n")

url = paste(server, "/api", "/invocations/", invocation_id, "/step_jobs_summary", sep="")
cat("get_invocation_step_jobs_summary: ", url, "\n")

url = paste(server, "/api", "/invocations/", invocation_id, "/report", sep="")
cat("get_invocation_report: ", url, "\n")
```

# Production des exemples (fichiers json ci-dessus):

**Shell**

```
SERVER='https://siwaa.toulouse.inrae.fr'
API_KEY='c47eaed277315947711b8cb5d5e4739c'
WORKFLOW_ID='28b1a3fca48be365'
INVOCATION_ID='9a7f1d7cd30e6721'
STEP_ID='07081866f13f77b2'

FILE="show_invocation.json"
URL=${SERVER}/api/invocations/${INVOCATION_ID}
curl --output ${FILE} --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:'${API_KEY} $URL

FILE="show_invocation_step.json"
URL=${SERVER}/api/invocations/${INVOCATION_ID}/steps/${STEP_ID}
curl --output ${FILE} --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:'${API_KEY} $URL

FILE="get_invocation_summary.json"
URL=${SERVER}/api/invocations/${INVOCATION_ID}/jobs_summary
curl --output ${FILE} --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:'${API_KEY} $URL

FILE="get_invocation_step_jobs_summary.json"
URL=${SERVER}/api/invocations/${INVOCATION_ID}/step_jobs_summary
curl --output ${FILE} --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:'${API_KEY} $URL


FILE="get_invocation_report.json"
URL=${SERVER}/api/invocations/${INVOCATION_ID}/report
curl --output ${FILE} --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:'${API_KEY} $URL

```

# Note

Utility of /workflows/workflow_id/ devant invocations/invocation_id ?
- cf https://siwaa.toulouse.inrae.fr/api/workflows/28b1a3fca48be365/invocations/9a7f1d7cd30e6721
- vs https://siwaa.toulouse.inrae.fr/api/invocations/9a7f1d7cd30e6721

