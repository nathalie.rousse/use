# Siwaa jobs

# Jobs 

Some URLs :
  - https://siwaa.toulouse.inrae.fr/api/jobs
  - https://siwaa.toulouse.inrae.fr/api/jobs/b8c5278982c5df19
  - https://siwaa.toulouse.inrae.fr/api/jobs/b8c5278982c5df19/metrics

Example:

- List of 'my' jobs :
  **https://siwaa.toulouse.inrae.fr/api/jobs**

  Extrait response list :
  ```json
  {
        "create_time": "2024-06-14T11:21:07.016152",
        "exit_code": 0,
        "galaxy_version": "22.01",
        "history_id": "fbc9b2a2ce1f7748",
        "id": "b8c5278982c5df19",
        "model_class": "Job",
        "state": "ok",
        "tool_id": "toolshed-siwaa.toulouse.inrae.fr/repos/patrick_chabrier/carboseq_s/CarboSeqSimulator/1.0.0",
        "update_time": "2024-06-14T11:22:40.876467"
  }
  ```

- The job id:"b8c5278982c5df19" :
  **https://siwaa.toulouse.inrae.fr/api/jobs/b8c5278982c5df19**

  Response :
  ```json
  {
    "command_line": "bash -e -c \"mkdir inputs && unzip -d input /nfs/work/record/galaxy/data/f/f/4/dataset_ff4568e5-c31d-42d2-8803-1517927f79c7.dat && set -a ;source /usr/local/src/myscripts/.env && export CARBOSEQPREPROCESSOR_INPUT_DIR=$(realpath -s input) export CSOPRALIBS_PARAM_OUTPUT=$(realpath -s output) && Rscript /usr/local/src/myscripts/scenarios/run-sim-perf/run.r && cd output && zip -r output.zip * && cp output.zip /lustre/siwaa/job_directory/007/7093/outputs/galaxy_dataset_085de6ca-5f70-4a2c-abbf-7a102cea8999.dat\"",
    "command_version": "",
    "create_time": "2024-06-14T11:21:07.016152",
    "exit_code": 0,
    "galaxy_version": "22.01",
    "history_id": "fbc9b2a2ce1f7748",
    "id": "b8c5278982c5df19",
    "inputs": {
        "input": {
            "id": "f15198678777f426",
            "src": "hda",
            "uuid": "ff4568e5-c31d-42d2-8803-1517927f79c7"
        }
    },
    "model_class": "Job",
    "output_collections": {},
    "outputs": {
        "output": {
            "id": "bf895db2e4bfe8d1",
            "src": "hda",
            "uuid": "085de6ca-5f70-4a2c-abbf-7a102cea8999"
        }
    },
    "params": {
        "__input_ext": "\"auto\"",
        "chromInfo": "\"/nfs/work/record/galaxy/var/tool-data/shared/ucsc/chrom/?.len\"",
        "dbkey": "\"?\""
    },
    "state": "ok",
    "tool_id": "toolshed-siwaa.toulouse.inrae.fr/repos/patrick_chabrier/carboseq_s/CarboSeqSimulator/1.0.0",
    "update_time": "2024-06-14T11:22:40.876467"
  }
  ```

- Metrics for the job id:"b8c5278982c5df19" :
  **https://siwaa.toulouse.inrae.fr/api/jobs/b8c5278982c5df19/metrics**

  Response :
  ```json
  [
    {
        "name": "galaxy_slots",
        "plugin": "core",
        "raw_value": "28.0000000",
        "title": "Cores Allocated",
        "value": "28"
    },
    {
        "name": "galaxy_memory_mb",
        "plugin": "core",
        "raw_value": "114688.0000000",
        "title": "Memory Allocated (MB)",
        "value": "114688"
    },
    {
        "name": "start_epoch",
        "plugin": "core",
        "raw_value": "1718364079.0000000",
        "title": "Job Start Time",
        "value": "2024-06-14 13:21:19"
    },
    {
        "name": "end_epoch",
        "plugin": "core",
        "raw_value": "1718364158.0000000",
        "title": "Job End Time",
        "value": "2024-06-14 13:22:38"
    },
    {
        "name": "runtime_seconds",
        "plugin": "core",
        "raw_value": "79.0000000",
        "title": "Job Runtime (Wall Clock)",
        "value": "1 minute"
    },
    {
        "name": "cpuacct.usage",
        "plugin": "cgroup",
        "raw_value": "144076177257.0000000",
        "title": "CPU Time",
        "value": "2 minutes"
    },
    {
        "name": "memory.failcnt",
        "plugin": "cgroup",
        "raw_value": "0E-7",
        "title": "Failed to allocate memory count",
        "value": 0
    },
    {
        "name": "memory.limit_in_bytes",
        "plugin": "cgroup",
        "raw_value": "9223372036854771712.0000000",
        "title": "Memory limit on cgroup (MEM)",
        "value": "8.0 EB"
    },
    {
        "name": "memory.max_usage_in_bytes",
        "plugin": "cgroup",
        "raw_value": "2633334784.0000000",
        "title": "Max memory usage (MEM)",
        "value": "2.5 GB"
    },
    {
        "name": "memory.memsw.limit_in_bytes",
        "plugin": "cgroup",
        "raw_value": "9223372036854771712.0000000",
        "title": "Memory limit on cgroup (MEM+SWP)",
        "value": "8.0 EB"
    },
    {
        "name": "memory.memsw.max_usage_in_bytes",
        "plugin": "cgroup",
        "raw_value": "2633334784.0000000",
        "title": "Max memory usage (MEM+SWP)",
        "value": "2.5 GB"
    },
    {
        "name": "memory.oom_control.oom_kill_disable",
        "plugin": "cgroup",
        "raw_value": "0E-7",
        "title": "OOM Control enabled",
        "value": "Yes"
    },
    {
        "name": "memory.oom_control.under_oom",
        "plugin": "cgroup",
        "raw_value": "0E-7",
        "title": "Was OOM Killer active?",
        "value": "No"
    },
    {
        "name": "memory.soft_limit_in_bytes",
        "plugin": "cgroup",
        "raw_value": "9223372036854771712.0000000",
        "title": "Memory softlimit on cgroup",
        "value": "8.0 EB"
    }
  ]

  ```

# Lien avec output data de Siwaa tool 'CarboSeqSimulator'

- output data :

  - history id:"fbc9b2a2ce1f7748"
  - output data id:"bf895db2e4bfe8d1"

- Dans info data retournee par URL 
  **https://siwaa.toulouse.inrae.fr/api/histories/fbc9b2a2ce1f7748/contents/bf895db2e4bfe8d1**
  on trouve **"creating_job": "b8c5278982c5df19"**

- URL
  **https://siwaa.toulouse.inrae.fr/api/histories/fbc9b2a2ce1f7748/contents/bf895db2e4bfe8d1**
  response :
  ```json
  {
    "accessible": true,
    "annotation": null,
    "api_type": "file",
    "copied_from_ldda_id": null,
    "create_time": "2024-06-14T11:21:07.021883",
    "created_from_basename": null,
    "creating_job": "b8c5278982c5df19",
    "data_type": "galaxy.datatypes.binary.CompressedZipArchive",
    "dataset_id": "153e631d35da500f",
    "deleted": false,
    "display_apps": [],
    "display_types": [],
    "download_url": "/api/histories/fbc9b2a2ce1f7748/contents/bf895db2e4bfe8d1/display",
    "extension": "zip",
    "file_ext": "zip",
    "file_name": "/nfs/work/record/galaxy/data/0/8/5/dataset_085de6ca-5f70-4a2c-abbf-7a102cea8999.dat",
    "file_size": 29343,
    "genome_build": "?",
    "hashes": [],
    "hda_ldda": "hda",
    "hid": 2,
    "history_content_type": "dataset",
    "history_id": "fbc9b2a2ce1f7748",
    "id": "bf895db2e4bfe8d1",
    "meta_files": [],
    "metadata_dbkey": "?",
    "misc_blurb": "28.7 KB",
    "misc_info": "Archive:  /nfs/work/record/galaxy/data/f/f/4/dataset_ff4568e5-c31d-42d2-8803-1517927f79c7.dat\n  inflating: input/crop.csv          \n  inflating: input/soil.csv          \n  inflating: input/units.csv         \n  adding: crop_all.csv (deflated 96%)\n  adding",
    "model_class": "HistoryDatasetAssociation",
    "name": "CarboSeq output (zip)",
    "peek": "Compressed zip file",
    "permissions": {
        "access": [
            "dd5aa30913f92886"
        ],
        "manage": [
            "dd5aa30913f92886"
        ]
    },
    "purged": false,
    "rerunnable": true,
    "resubmitted": false,
    "sources": [],
    "state": "ok",
    "tags": [],
    "type": "file",
    "type_id": "dataset-bf895db2e4bfe8d1",
    "update_time": "2024-06-14T11:22:40.736702",
    "url": "/api/histories/fbc9b2a2ce1f7748/contents/bf895db2e4bfe8d1",
    "uuid": "085de6ca-5f70-4a2c-abbf-7a102cea8999",
    "validated_state": "unknown",
    "validated_state_message": null,
    "visible": true,
    "visualizations": [
        {
            "description": "Manually edit text",
            "embeddable": false,
            "entry_point": {
                "attr": {},
                "file": "editor.mako",
                "type": "mako"
            },
            "groups": null,
            "href": "/plugins/visualizations/editor/show",
            "html": "Editor",
            "logo": null,
            "name": "editor",
            "settings": null,
            "specs": null,
            "target": "galaxy_main",
            "title": null
        }
    ]
  }

  ```
