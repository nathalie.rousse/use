# Appel Workflow CarboSeq (travaux avant dans )

Travaux avant integration dans paquet R
(projet 'CarboSeq SIWAA API' https://forgemia.inra.fr/record/carboseqsiwaa)


- **Workflow 'MY_CarboSeqWFlightOutputs'**
  = import de workflow 'CarboSeqWFlightOutputs' + modifications :

  - Ajout *Input dataset* 'main_input'
  - Ajout *Simple inputs used for workflow logic* : 'chunck_size'
  - Label 'main_output' donne a la sortie :
    *Configure Output: 'output'* -> *Label*

- Data **INPUTS/400Sites.zip** ... : downloaded from 'Données partagées'
  Libraries -> CarboSeq Datasets -> BenchMarking

**Code** [workflow.R](./workflow.R)

