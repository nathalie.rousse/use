# load .RData from Siwaa

- The uploaded_to_siwaa/essai.RData file has been uploaded to Siwaa :
  - history_id: "1d51944702fdda60" , history name: "ESSAI RData"
  - rdata_id: "0cfbf335b8766ed0"   , .RrDdata data name: "essai.RData"


- R code that :
  1. download in memory the data "essai.RData" from history "ESSAI RData".
  2. load it.
  3. Use it.

```R

library(httr)

SERVER <- "https://siwaa.toulouse.inrae.fr"
API_KEY <- '9e38ceeedc3c14a215bba53273ff1384'

history_id <- "1d51944702fdda60" # history name: "ESSAI RData"
rdata_id <- "0cfbf335b8766ed0"   # .Rdata data name: "essai.RData"

get_data_info <- function(data_id, history_id, server=SERVER, api_key=API_KEY){
    hda_content_url = paste(server, "/api", "/histories/", history_id,
                            "/contents/", data_id, sep="")
    r <- httr::GET(url=hda_content_url,
                   httr::add_headers(.headers=c("x-api-key"=api_key)))
    data_info = httr::content(r)
    return(data_info)
}

download_in_memory_data <- function(data_info, server=SERVER, api_key=API_KEY){

    hda_download_url = paste(server, data_info$download_url, sep="")
    r <- httr::GET(url=hda_download_url,
                   httr::add_headers(.headers=c("x-api-key"=api_key)))
    data = httr::content(r)
    return(data)
}

# 1. Download in memory
rdata_info = get_data_info(data_id=rdata_id, history_id=history_id)
rdata = download_in_memory_data(data_info=rdata_info)

# 2. load rdata
temp_file <- tempfile(fileext=".RData")
writeBin(rdata, temp_file)
load(temp_file)

# 3. Use essai.RData :

d = MY_DATA_FROM_RDATA
cat("\n\n"); cat("MY_DATA_FROM_RDATA from essai.RData :", d)
ret = hello("toto")

```
