# [CarboSeqSimulator_tool.R](./CarboSeqSimulator_tool.R)

# Desc

- **[CarboSeqSimulator_tool.R](./CarboSeqSimulator_tool.R) contains methods 
  allowing to command Siwaa tool 'CarboSeqSimulator' from R code.**

- CarboSeqSimulator_tool.R requires **R libraries** : '**httr**', '**rjson**'

- CarboSeqSimulator_tool.R
  exposes **'external methods' destinated to the user**, and contains also
  some 'internal methods' written to be called by external methods.

- **Configuration according to your own case** :

  The '**CONFIGURATION**' part of CarboSeqSimulator_tool.R contains some
  *constants* used as default values by external methods of
  CarboSeqSimulator_tool.R.

  Before using CarboSeqSimulator_tool.R, you can choose to update its
  'CONFIGURATION' values according to your own case. Otherwise, give your 
  own values by parameters of external methods.

- **Prerequisite** :
  - an available **API_KEY** value is required (cf
    '[How to get an available API KEY value](/../siwaa-api/token/README.md)')
  - a local available **input file** 
    (such as [40Sites.zip](./../INPUTS/40Sites/40Sites.zip))
    is required (cf 'input_file_path' parameter of 'run_sim' method).

# Sequence using CarboSeqSimulator_tool.R

Here is an example of using CarboSeqSimulator_tool.R methods to command
Siwaa tool 'CarboSeqSimulator' :
- 'run_sim' : upload input file and run tool
- 'wait_sim_results' : survey process, wait for the output data to be ready
- 'download_file' : download the output file (once ready)
- *'download_in_memory_data' : download the output data in memory (once ready)*
- 'clear_history_of_data_info' : clear history

```R
source("CarboSeqSimulator_tool.R") # path to CarboSeqSimulator_tool.R

MY_API_KEY = '9e38ceeedc3c14a215bba53273ff1384'

output_file_data_info <- run_sim(input_file_path="../INPUTS/40Sites/40Sites.zip", api_key=MY_API_KEY)

ret_survey = wait_sim_results(data_info=output_file_data_info, api_key=MY_API_KEY)
cat("ret_survey$cr: ", ret_survey$cr, "\n")

# At this step :
# -  If ret_survey$cr is "END_DATA_NOT_READY", then STOP.
# -  If ret_survey$cr is "NOT_FINISHED", then you can call again 'wait_sim_results' to continue survey
# -  If ret_survey$cr is "END_DATA_READY", then you can download results.

# You can continue only if ret_survey$data_state is 'ok' !!!

output_file_data_info = ret_survey$data_info
cat("output_file_data_info state: ", output_file_data_info$state, "\n")

# Download the output data file
ret_file = download_file(data_info=output_file_data_info, api_key=MY_API_KEY)
if (ret_file != "") output_file_path = ret_file

# Download the output data in memory
ret_data = download_in_memory_data(data_info=output_file_data_info, api_key=MY_API_KEY)
if (ret_data$cr_ok) output_file_data = ret_data$value

clear_history_of_data_info(output_file_data_info)

```

# Some more details

- 'run_sim' -> **'history_name'** parameter :

  'run_sim' with creation of a history **named "HIST from more details"** :
  ```R
  output_file_data_info <- run_sim(input_file_path="../../INPUTS/40Sites/40Sites.zip", history_name="HIST from more details", api_key=MY_API_KEY)
  ```

- 'run_sim' -> **'history_id'** parameter :

  'run_sim' using **an existing history** :
  ```R
  output_file_data_info <- run_sim(input_file_path="../../INPUTS/40Sites/40Sites.zip", history_id="4f93aea6ec2af23e", api_key=MY_API_KEY)
  ```

- 'ret_survey' -> **'delay'** and **'duration_max'** parameters :

  Survey **during duration_max=20 seconds** by **step of delay=5 seconds**:
  ```R
  ret_survey = wait_sim_results(data_info=output_file_data_info, delay=5, duration_max=20, api_key=MY_API_KEY)
  ```

- 'ret_survey' -> Survey (with 'delay' and 'duration_max' parameters
  default values) :
  ```R
  ret_survey = wait_sim_results(data_info=output_file_data_info, api_key=MY_API_KEY)
  ```

- 'download_file' -> **'outputs_folder'** parameter :

  Download the tool result file **into folder "AAA/BBB/CCC"**
  (that must exist!) :
  ```R
  ret_file = download_file(data_info=output_file_data_info, outputs_folder="AAA/BBB/CCC", api_key=MY_API_KEY)
  ```

- 'download_file', 'download_in_memory_data' will *fail* if output data not ready (output_file_data_info$state not "ok").

