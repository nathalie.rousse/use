# Archiver sur Software Heritage, déposer sur HAL

### Resources
 
  - **HAL**

    (a) "Déposer le code source d'un logiciel dans HAL- Bonnes pratiques et
    guide utilisateur"
    https://inria.hal.science/hal-01872189v2/file/create%20deposit%20fr%20-%20separated%20scenario.pdf

  - **SWH**

    (b) "How to archive and reference your code" 
    https://www.softwareheritage.org/how-to-archive-reference-code

    (d) Video francais (partie intéressante à partir de 8:50) (SWH et publi) 
    https://www.youtube.com/watch?v=C2WKzMe2PRk

    (e) Video anglais (partie intéressante à partir de 9:00) (SWH et publi) :
    https://www.youtube.com/watch?v=8nlSvYh7VpI

  - Illustration

    (c) Video (à partir de 18:25)
    "Dépôt d’un package R sur Software Heritage et référencement sur HAL"
    https://videos.univ-grenoble-alpes.fr/video/29685-depot-dun-package-r-sur-software-heritage-et-referencement-sur-hal/

### Etapes

Code sur forge MIAT --> archiver sur SWH --> déposer dans HAL

- **[Forge] Preparer code :**
  
  - Fichiers README, AUTHORS, LICENSE à la racine du projet (voir (a) et (b)).

  - Fichier codemeta.json :

    - codemeta.json contient metadata
      (pouvant se substituer aux 3 fichiers ci-dessus (~?)).
      codemeta.json est exploitable lors du dépôt dans HAL.
    - Creation de codemeta.json :
      - On peut utiliser **CodeMeta generator tool** :
        https://codemeta.github.io/codemeta-generator
      - Dans le cas d'un code de type paquet R, on peut utiliser le **paquet R
        'codemetar'** qui permet de générer un fichier codemeta.json à
        partir du fichier DESCRIPTION du paquet R.
        Illustration : voir video (c).

- **[SWH] archiver le code dans SWH :**

   - *'Save code Now'* => code archivé.

   -  Une fois archivé, le code dispose d'identifiants SWHIDs qui permettront
      de le référencer.
   
      **Software Heritage intrinsic identifiers (SWHID)** :
      SWHID is a fully documented standard identifier schema, that equips
      any software artifact (files...) with intrinsic identifiers.
      SWHIDs are 'identifiers of choice for reproducibility'.

- **[HAL] déposer le logiciel dans HAL, à partir de son archive dans SWH :**

   - *'Chargez les métadonnées à partir d'un identifiant'* :
     donner le SWHID 'principal' (de HEAD)

   - *'Récupérer les métadonnées'* => récupération par HAL de metadata dans
     codemeta.json.

### Case of carboseqsiwaa

**carboseqsiwaa : paquet R sur forge MIAT
https://forgemia.inra.fr/record/carboseqsiwaa**

- **[Forge] Preparation du code (codemeta.json)** :

  Il s'agit d'un paquet R
  => utilisation du **paquet R 'codemetar'** pour générer codemeta.json :
  ```
  # install.packages("devtools")
  devtools::install_github("ropensci/codemetar")
  
  # generation (at root)
  codemetar::write_codemeta()
  ```
  Note : L'information 'Affiliation' est absente de codemeta.json,
  absente de DESCRIPTION. Elle sera saisie directement lors du dépôt dans HAL.

- **[SWH] Archivage du code dans SWH :**

   *'Save code Now'* : 'git' 'https://forgemia.inra.fr/record/carboseqsiwaa'

   => code carboseqsiwaa archivé dans SWH.

- **[HAL] déposer le logiciel dans HAL :**
  
  Les informations sont complétées manuellement, en particulier ajout de
  l'information 'Affiliation' :
  'Unité de Mathématiques et Informatique Appliquées de Toulouse"

## Faire reference au code qui a ete archive dans SWH :

Exemple avec le code **carboseqsiwaa** 
(https://forgemia.inra.fr/record/carboseqsiwaa).

- SWHID du projet entier (HEAD) :
  ```
  swh:1:dir:b401d1964c283805508e2c36efa37d199acb992f;origin=https://forgemia.inra.fr/record/carboseqsiwaa;visit=swh:1:snp:7c2e304ebba106a4ea4f3070d755ee0071a1c1c1;anchor=swh:1:rev:e4952302df19def3b93d9f1f9139ea74af3c9d30
  ```

  => Lien vers l'archive SWH de carboseqsiwaa :

  https://archive.softwareheritage.org/swh:1:dir:b401d1964c283805508e2c36efa37d199acb992f;origin=https://forgemia.inra.fr/record/carboseqsiwaa;visit=swh:1:snp:7c2e304ebba106a4ea4f3070d755ee0071a1c1c1;anchor=swh:1:rev:e4952302df19def3b93d9f1f9139ea74af3c9d30

- Code fragment

  SWHID du fichier deploy_branch.sh :
  ```
  swh:1:cnt:8bd93097193c0a336405ef906df6f267b6fb3a7e;origin=https://forgemia.inra.fr/record/carboseqsiwaa;visit=swh:1:snp:7c2e304ebba106a4ea4f3070d755ee0071a1c1c1;anchor=swh:1:rev:e4952302df19def3b93d9f1f9139ea74af3c9d30;path=/deploy_branch.sh
  ```

  => Lien dans l'archive SWH de carboseqsiwaa vers le fichier deploy_branch.sh
  (with the relevant lines highlighted) :

  https://archive.softwareheritage.org/swh:1:cnt:8bd93097193c0a336405ef906df6f267b6fb3a7e;origin=https://forgemia.inra.fr/record/carboseqsiwaa;visit=swh:1:snp:7c2e304ebba106a4ea4f3070d755ee0071a1c1c1;anchor=swh:1:rev:e4952302df19def3b93d9f1f9139ea74af3c9d30;path=/deploy_branch.sh;lines=12-23

* SWHID de la branch 'dev' :
  ```
  swh:1:dir:bb9386e855751aeb0f6b4687f8a6303a5e335f07;origin=https://forgemia.inra.fr/record/carboseqsiwaa;visit=swh:1:snp:7c2e304ebba106a4ea4f3070d755ee0071a1c1c1;anchor=swh:1:rev:6f4c7953e975e0b183a5df863b4f3bfb799128e3
  ```

  Lien dans l'archive SWH de carboserqsiwaa vers sa branch 'dev' :

  https://archive.softwareheritage.org/swh:1:dir:bb9386e855751aeb0f6b4687f8a6303a5e335f07;origin=https://forgemia.inra.fr/record/carboseqsiwaa;visit=swh:1:snp:7c2e304ebba106a4ea4f3070d755ee0071a1c1c1;anchor=swh:1:rev:6f4c7953e975e0b183a5df863b4f3bfb799128e3

- Voir video (d).

## Misc

- See also :
  https://archive.softwareheritage.org/browse/origin/https://forgemia.inra.fr/record/carboseqsiwaa

- Note : dans la video (c) on voit la vignette software heritage dans la page
  de la forge, sur le rebord droit (couleur verte, jaune...) :
  ça a l'air specifique de github (absent avec gitlab).

