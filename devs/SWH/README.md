# Software heritage


### **Software Heritage**

  - Accueil : **https://docs.softwareheritage.org**
  - **swh-web browse** (https://archive.softwareheritage.org/browse):
    graphical interface that eases the navigation in the archive.
  - **swh-web API** (https://archive.softwareheritage.org/api): enables to
    query the content of the archive through HTTP requests and get responses
    in JSON or YAML.

### **[Archiver sur Software Heritage, déposer sur HAL](SWH_archive_reference_HAL.md)**

  - Resources for HAL, SWH (links, videos...)
  - Steps : Code sur forge --> archiver sur SWH --> déposer dans HAL
  - Faire référence au code qui a ete archivé dans SWH

### **[Software Heritage API](SWH_API.md)**

  - Resources for SWH API
  - Use the API :
    - Search an archived repository, identify its visits. 
    - For one visit of an archived repository : download from the archive
      the corresponding git repository, explore or download the content of
      one of its branches. 
