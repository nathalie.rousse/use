# Software Heritage API

## Resources

- **swh-web API** (**https://archive.softwareheritage.org/api**): enables to
  query the content of the archive through HTTP requests and get responses in
  JSON or YAML.

- Software Heritage **API
  https://archive.softwareheritage.org/api/1 entry point**.

- Getting Started with the Software Heritage API
  https://docs.softwareheritage.org/devel/getting-started/api.html

## Use the API

See **[Getting Started with the Software Heritage API](https://docs.softwareheritage.org/devel/getting-started/api.html)**

**Shell Commands** using :
- **curl** (or **wget**) to call the API.
- **jq** (https://jqlang.github.io/jq/manual) to manage json data.
  See also https://jqplay.org.

Main Software Heritage server : https://archive.softwareheritage.org/ basename.
All API calls will be forged according to the same syntax: https://archive.softwareheritage.org/api/1/ *endpoint*.


### Init-Config

```
SWH_SERVER="https://archive.softwareheritage.org/" # Main Software Heritage server
SWH_API_BASE=${SWH_SERVER}"api/1/"

# Utils
function getval { # returns input without '"'
   s=$1; s="${s%\"}"; s="${s#\"}"; val=$s; echo $val
}

cd example_API
```

### General

- **SWH Statistics :**
  ```
  curl ${SWH_API_BASE}stat/counters/
  # ie
  curl https://archive.softwareheritage.org/api/1/stat/counters/
  ```

- **Search for a keyword :**
  ```
  curl ${SWH_API_BASE}origin/search/inrae/                 # keyword 'inrae'
  curl ${SWH_API_BASE}origin/search/inrae/ | jq .          # prettier
  curl ${SWH_API_BASE}origin/search/galaxy/?limit=12 | jq .

  # ---------------------------------------------------------------------------
  # example with 'carboseq' keyword

  name="carboseqsiwaa"
  curl ${SWH_API_BASE}origin/search/${name}/ | jq .

  # save into file the search result
  curl ${SWH_API_BASE}origin/search/${name}/ | jq . > ${name}_search.json

  # ---------------------------------------------------------------------------
  # List of the archive's analysed repositories corresponding with a keyword ('url' field)

  name="carboseq" # keyword 'carboseq'
  curl ${SWH_API_BASE}origin/search/${name}/ | jq -r '.[].url'

  ```
  **ILLUSTRATION :
  [the Search 'carboseqsiwaa' information (carboseqsiwaa_search.json)](example_API/carboseqsiwaa_search.json)**

### One specific repository

**Case/example of 'carboseqsiwaa' repository
https://forgemia.inra.fr/record/carboseqsiwaa**
```
origin_url="https://forgemia.inra.fr/record/carboseqsiwaa"
name="carboseqsiwaa"    # used for files names
```
- Get information of the repository :
  ```
  curl ${SWH_API_BASE}origin/${origin_url}/get/ | jq .
  ```

- **Visits** of the repository
  (when the repository has been analysed by the archive bots) :
  ```
  # visits information
  curl ${SWH_API_BASE}origin/${origin_url}/visits/ | jq .

  visits=$(curl ${SWH_API_BASE}origin/${origin_url}/visits/)

  # dates of the visits
  echo $visits | jq -r '.[].date'
  ```

- The **latest visit** of the repository :
  ```
  # the latest visit information
  latest_visit=$(curl ${SWH_API_BASE}origin/${origin_url}/visit/latest/)

  echo $latest_visit | jq .    # affichage
  echo $latest_visit | jq . > ${name}_latest_visit.json # save into file

  # the date of the latest visit of the repository
  echo $latest_visit | jq '.date'
  ```
  **ILLUSTRATION : [the latest visit information (carboseqsiwaa_latest_visit.json)](example_API/carboseqsiwaa_latest_visit.json) of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

### One visit of one repository 

**Example/case of
[the latest visit](example_API/carboseqsiwaa_latest_visit.json)**
of **the 'carboseqsiwaa' repository
https://forgemia.inra.fr/record/carboseqsiwaa**
```
visit=$latest_visit    # latest_visit from above
```

- The **snapshot information** of the visit :
  ```
  # Extract the snapshot_id from the visit
  snapshot_id=$(echo $visit | jq '.snapshot')
  echo $snapshot_id

  # Extract the snapshot_url from the visit
  snapshot_url=$(echo $visit | jq '.snapshot_url')
  echo $snapshot_url

  # Get the snapshot information of the visit
  URL=$(getval ${snapshot_url})
  snapshot=$(curl ${URL})

  echo $snapshot | jq .    # affichage
  echo $snapshot | jq .  > ${name}_latest_visit_snapshot.json # save into file

  ```
  **ILLUSTRATION :
  [the snapshot information (carboseqsiwaa_latest_visit_snapshot.json)](example_API/carboseqsiwaa_latest_visit_snapshot.json) of the latest visit of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

- **Download the git repository** corresponding with the visit :

  '**git-bare**' *([more](https://docs.softwareheritage.org/devel/swh-vault/api.html#git-bare))* to download a .tar.gz file containing a real git repository (.git folder). This allows to recreate original git repository, including branches.

  ```
  SWHID="swh:1:snp:$(getval ${snapshot_id})"
  URL=${SWH_API_BASE}vault/git-bare/${SWHID}/ # to order (POST) and then survey (GET)

  # Order the meal (POST)
  order=$(curl -X POST ${URL})
  echo $order | jq  .

  # Survey end of processing : Ask if ready until status value: 'done'
  #curl ${URL} | jq .
  survey=$(curl ${URL})
  status=$(echo $survey | jq '.status') # values : 'new', 'pending', 'done'...
  echo ${status}

  # Once the processing finished (ie status value 'done') : Get the plate
  fetch_url=$(echo $order | jq '.fetch_url') # downloading url (from $order or $survey)
  FILE_targz="${name}_latest_visit_gitproject.tar.gz"
  curl -L $(getval ${fetch_url}) -o ${FILE_targz} # "-L" required !!! (or wget instead of curl)
  ```
  **ILLUSTRATION :
  [the .tar.gz of the git repository (carboseqsiwaa_latest_visit_gitproject.tar.gz)](example_API/carboseqsiwaa_latest_visit_gitproject.tar.gz) of the latest visit of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

  ```
  # Extract the .git folder
  tar -xvf ${FILE_targz}
  git_folder="${SWHID}.git" # (information from SWH)
  echo $git_folder

  # Explore its content like a normal ("non-bare") git repository by cloning it
  git clone $git_folder
  ```

### One branch of one visit of one repository 

**Example/case of 'dev' branch**  of 
**[the latest visit](example_API/carboseqsiwaa_latest_visit.json)
of the 'carboseqsiwaa' repository
https://forgemia.inra.fr/record/carboseqsiwaa**
```
branch_name="refs/heads/dev"    # cf snapshot content
```

- The **revision information** of the **branch** of the visit :

  Into **[the snapshot information](example_API/carboseqsiwaa_latest_visit_snapshot.json)** of the visit, the **revision information** of each **branch** can be retrieved by following its **'target_url'** link.

  ```
  # Extract (from snapshot information) the 'target_url' of the branch
  branch_target_url=$(echo ${snapshot} | jq '.branches' | jq '."'${branch_name}'"' | jq '.target_url')

  # Get the revision information of the branch
  branch_revision_information=$(curl $(getval $branch_target_url))

  echo $branch_revision_information | jq .    # affichage
  echo $branch_revision_information | jq . > ${name}_latest_visit_dev_branch_revision.json # save into file
  ```
  **ILLUSTRATION :
  [the revision information (carboseqsiwaa_latest_visit_dev_branch_revision.json)](example_API/carboseqsiwaa_latest_visit_dev_branch_revision.json) of 'dev' branch of the latest visit of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

- The **root directory** of the **branch** of the visit :

  The **revision information** of the branch contains the **root directory
  of the project** that can be explored (files, directories) by following the
  **'directory_url'** link.

  ```
  # Extract 'directory_url' of the branch (from branch revision information) 
  branch_directory_url=$(echo $branch_revision_information | jq '.directory_url')

  # Get the root
  root=$(curl $(getval $branch_directory_url))

  echo $root | jq .    # affichage
  echo $root | jq .  > ${name}_latest_visit_dev_branch_root.json # save into file
  ```
  **ILLUSTRATION :
  [the root (carboseqsiwaa_latest_visit_dev_branch_root.json)](example_API/carboseqsiwaa_latest_visit_dev_branch_root.json) of 'dev' branch of the latest visit of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

- **Explore content** of the **root directory** of the branch :
  ```
  echo "List of contents and directories located at the root of 'dev' branch:"
  echo ${root} | jq -r '.[].name'

  echo "Only files:"
  echo ${root} | jq '.[] | select(.type == "file")' | jq -r '.name'

  echo "Only dirs:"
  echo ${root} | jq '.[] | select(.type == "dir")' | jq -r '.name'
  echo "... and their 'target_url':" # ... to continue exploration
  echo ${root} | jq '.[] | select(.type == "dir")' | jq -r '.target_url'
  ```

- **Download content** of the **root directory** of the branch :

  '**Flat**' *([more](https://docs.softwareheritage.org/devel/swh-vault/api.html#flat))*

  ```
  # Extract 'directory' (id) of the branch (from branch revision information) 
  branch_directory=$(echo $branch_revision_information | jq '.directory')

  SWHID="swh:1:dir:$(getval ${branch_directory})"
  URL=${SWH_API_BASE}vault/flat/${SWHID}/ # to order (POST) and then survey (GET)

  # Order the meal (POST)
  order=$(curl -X POST ${URL})
  echo $order | jq  .

  # Survey end of processing : Ask if ready until status value: 'done'
  #curl ${URL} | jq .
  survey=$(curl ${URL})
  status=$(echo $survey | jq '.status') # values : 'new', 'pending', 'done'...
  echo ${status}

  # Once the processing finished (ie status value 'done') : Get the plate
  fetch_url=$(echo $order | jq '.fetch_url') # downloading url (from $order or $survey)
  FILE_targz="${name}_latest_visit_dev_branch_root.tar.gz"
  curl -L $(getval ${fetch_url}) -o ${FILE_targz} # "-L" required !!! (or wget instead of curl)
  ```
  **ILLUSTRATION :
  [the .tar.gz of the root directory (carboseqsiwaa_latest_visit_dev_branch_root.tar.gz)](example_API/carboseqsiwaa_latest_visit_dev_branch_root.tar.gz) of the 'dev' branch of the latest visit of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

  ```
  # Extract the root folder
  tar -xvf ${FILE_targz}
  root_folder="${SWHID}" # (information from SWH)

  echo "$root_folder : root directory of the branch" 
  ```

- **Download one file** of the branch :

  **Example/case of 'DESCRIPTION', 'LICENSE.md', '.gitlab-ci.yml' files**
  of **'dev' branch
  ([root](example_API/carboseqsiwaa_latest_visit_dev_branch_root.json))
  of the latest visit of
  'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

  ```
  filename=".gitlab-ci.yml"
  file_target_url=$(echo ${root} | jq '.[]' | jq 'select(.type == "file")' | jq 'select(.name == "'${filename}'")' | jq '.target_url') # extract file 'target_url' (from root)
  file_download_url=$(getval ${file_target_url})raw/ # 'data_url' from file_target_url info
  echo ""
  echo "*** file $filename content ***"
  echo ""
  curl ${file_download_url} # affichage
  echo "------------------------------"

  filename="LICENSE.md"
  file_target_url=$(echo ${root} | jq '.[]' | jq 'select(.type == "file")' | jq 'select(.name == "'${filename}'")' | jq '.target_url') # extract file 'target_url' (from root)
  file_download_url=$(getval ${file_target_url})raw/ # 'data_url' from file_target_url info
  echo ""
  echo "*** file $filename content ***"
  echo ""
  curl ${file_download_url} # affichage
  echo "------------------------------"

  filename="DESCRIPTION"
  file_target_url=$(echo ${root} | jq '.[]' | jq 'select(.type == "file")' | jq 'select(.name == "'${filename}'")' | jq '.target_url') # extract file 'target_url' (from root)
  file_download_url=$(getval ${file_target_url})raw/ # 'data_url' from file_target_url info
  curl ${file_download_url} -o __downloaded__${filename} # save into file
  ```

  **ILLUSTRATION :
  [DESCRIPTION file](example_API/__downloaded__DESCRIPTION) of 'dev' branch of the latest visit of 'carboseqsiwaa' repository https://forgemia.inra.fr/record/carboseqsiwaa**

