#!/bin/bash

# REDELAC

# 07/03/24
# https://forgemia.inra.fr/redelac/redelac-toolbox/-/issues/65
# Un outil pour faciliter le transfert d'une liste de collections de liste d'archives zip sur le cloud INRAE sous la forme d'une archive zip unique.

# Shell Commands using :
# - Siwaa API and curl (to send/receive Siwaa API requests).
# - jq (https://jqlang.github.io/jq/manual) to manage json data.
#       https://jqplay.org
# - wget

###############################################################################
# Initialisation and functions

OUTPATH='./factory'
if [ -d $OUTPATH ]
then
    echo "Warning : Existing $OUTPATH folder"
else
    echo "Create $OUTPATH folder"
    mkdir $OUTPATH
fi

SERVER='https://siwaa.toulouse.inrae.fr'

API_KEY="9e38ceeedc3c14a215bba53273ff1384"
# or token value previously saved into TOKEN file
#API_KEY=$(cat TOKEN)

CONTENT_TYPE="Content-Type: application/x-www-form-urlencoded"

# Histories => $OUTPATH/histories.json
function get_histories {

    curl --output $OUTPATH/histories.json \
         --header '${CONTENT_TYPE}' --header 'x-api-key:'${API_KEY} \
         ${SERVER}/api/histories

    echo "histories name, id:"
    cat $OUTPATH/histories.json | jq '.[] | .name, .id'
}

# Liste des url 'download' de chaque hda de chaque collection d'une history
# Commence par identifier les collections de history_id
# Note : history collections (la 3e data "Job" ?)
function get_hda_download_url_list {

    history_id=$1

    history_content=$(curl --header '${CONTENT_TYPE}' \
                           --header 'x-api-key:'${API_KEY} \
                           ${SERVER}/api/histories/${history_id}/contents)

    # filter history content data being collections
    history_collections=$(echo $history_content | \
        jq '.[] | select(.type == "collection") | select(.collection_type == "list") | select(.job_source_type != "Job")')

    # each collection
    for contents_url in $(echo $history_collections | jq -r '.contents_url'); do

        history_collection_content=$(curl --header '${CONTENT_TYPE}' \
                                          --header 'x-api-key:'${API_KEY} \
                                          ${SERVER}${contents_url})

        # each data of collection content
        for hda_id in $(echo $history_collection_content | \
                             jq -r '.[].object.id'); do
            hda_download_url=${SERVER}/api/histories/${history_id}/contents/${hda_id}/display
            echo $hda_download_url
        done
    done
}

# Liste des url 'download' de chaque collection d'1 history
# Note : !!! Taille pour les downloads...
function get_collection_download_url_list {
    history_id=$1
    history_content=$(curl --header '${CONTENT_TYPE}' \
                           --header 'x-api-key:'${API_KEY} \
                           ${SERVER}/api/histories/${history_id}/contents)
    history_collections=$(echo $history_content | \
        jq '.[] | select(.type == "collection") | select(.collection_type == "list") | select(.job_source_type != "Job")')
    for id in $(echo $history_collections | jq -r '.id'); do
        collection_download_url=${SERVER}/api/hda_collections/${id}/download
        echo $collection_download_url
    done
}

###############################################################################

# Histories => $OUTPATH/histories.json
get_histories

history_id="fffb62a146375ea6"

# liste des url 'download' de chaque collection de history_id
# (unused)
get_collection_download_url_list $history_id > $OUTPATH/collection_download_url_list_$history_id.txt

# liste des url 'download' de chaque hda de chaque collection de history_id
get_hda_download_url_list $history_id > $OUTPATH/hda_download_url_list_$history_id.txt

# telecharger les URL from .txt file => $OUTPATH/hda_from_collections_$history_id/display* zip files
wget -P $OUTPATH/HDA_from_collections_$history_id -i $OUTPATH/hda_download_url_list_$history_id.txt

# unzip
cd $OUTPATH
for hda_zip_file in $(ls HDA_from_collections_$history_id/display*); do
    unzip $hda_zip_file
done
cd ..

# zip
cd $OUTPATH
zip -r analyse_simulations_STICS.zip analyse_simulations_STICS
cd ..

###############################################################################

