# REDELAC

# Desc

07/03/24 :
- https://forgemia.inra.fr/redelac/redelac-toolbox/-/issues/65
- Un outil pour faciliter le transfert d'une liste de collections de liste d'archives zip sur le cloud INRAE sous la forme d'une archive zip unique.

**Shell Commands** using :
  - **Galaxy** side :
    - **Siwaa API** (called by **curl**, **wget**).
    - **jq** (https://jqlang.github.io/jq/manual) to manage json data.
      See also https://jqplay.org.
  - **NextCloud** side :
    - **NextCloud API** (called by **curl**, using **WEBDAV** protocol).

# Shell script files

## 1. Galaxy step

Script **[download_from_galaxy.sh](./download_from_galaxy.sh)** :

  - Desc : 'download_from_galaxy' downloads from Galaxy datasets (.zip files)
    of some collections of some histories, then gathered into a local
    'analyse_simulations_STICS.zip' file.

  - Input (-P argument) : list of the history_id:Collection_hid
  - Output : the final analyse_simulations_STICS.zip file 
    under the output folder (-O argument) 
    *(+ analyse_simulations_STICS folder, hda_[History]_[Collection]_[hda].zip)*

  - Example :

    ```sh
    ./download_from_galaxy.sh -S "https://siwaa.toulouse.inrae.fr" -k "9e38ceeedc3c14a215bba53273ff1384" -O "redelac_results" -P "fffb62a146375ea6:21 fffb62a146375ea6:11"

    # if config.json (default configuration file) right updated
    ./download_from_galaxy.sh -O "redelac_results" -P "fffb62a146375ea6:21 fffb62a146375ea6:11"
    ```

  - For more  :
    ```sh
    ./download_from_galaxy.sh --help
    ```

## 2. NextCloud step

Script **[upload_to_nextcloud.sh](./upload_to_nextcloud.sh)** :

  - Desc : 'upload_to_nextcloud' uploads a local file to NextCloud.

  - Input (-F argument) : local file to be uploaded.
  - Output (-R argument) : the remote file uploaded into NextCloud.

  - Example :

    ```sh
    ./upload_to_nextcloud.sh -D "https://nextcloud.inrae.fr" -u "jdupond" -p "dfgHu-9sE0f-P4dfM-O6LFG-PJjvY" -F "redelac_results/analyse_simulations_STICS.zip" -R "record-collab/SiwaaAPI/REDELAC_1/0318_analyse_simulations_STICS.zip"

    # if config.json (default configuration file) right updated
    ./upload_to_nextcloud.sh -F "redelac_results/analyse_simulations_STICS.zip" -R "record-collab/SiwaaAPI/REDELAC_1/0318_analyse_simulations_STICS.zip"
    ```

  - For more  :
    ```sh
    ./upload_to_nextcloud.sh --help
    ```

# Configuration

Both script files 
[download_from_galaxy.sh](./download_from_galaxy.sh) and
[upload_to_nextcloud.sh](./upload_to_nextcloud.sh) 
require some configuration information.

A configuration data can be given :

  - **by argument**

    Example for the server url :
    ```sh
    ./download_from_galaxy.sh -S "https://siwaa.toulouse.inrae.fr" ...
    ./upload_to_nextcloud.sh -D "https://nextcloud.inrae.fr" ...
    ```

  - **into a configuration file** :

    If a configuration data lacks (at least one) among arguments,
    then a configuration file is expected (given with -C argument).
    If a configuration file also lacks, then scripts use the 
    [config.json](./config.json) default configuration file.

The **configuration file** is a JSON file containing :
 
  - for Galaxy side (download_from_galaxy.sh script) :
    ```json
    {
      "galaxy":{ "GALAXY_SERVER":"https://siwaa.toulouse.inrae.fr",
                 "API_KEY":"9e38ceeedc3c14a215bba53273ff1384"}
    }
    ```

  - for NextCloud side (upload_to_nextcloud.sh script) :
    ```json
    {
      "nextcloud":{ "NEXTCLOUD_SERVER":"https://nextcloud.inrae.fr",
                    "LDAPNAME":"jdupond",
                    "APPPASSWORD":"dfgHu-9sE0f-P4dfM-O6LFG-PJjvY"}
    }
    ```

# More

- Some other information, examples about calling Galaxy API :
  [misc.md](./misc.md)
- Using [Siwaa API](/../siwaa-api/README.md).
- Using [NextCloud INRAE](/../nextcloud/README.md).

- Previous work:
  - Shell script :
    [download_hda_collections.sh](./previous/download_hda_collections.sh)
  - Run :
    ```sh
    ./download_hda_collections.sh
    ```

