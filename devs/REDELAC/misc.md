# Misc

## Token

- How to get an available API KEY value : see '[Token](/token)'.

## Histories

  ```sh
  # All histories owned by the user
  curl --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:9e38ceeedc3c14a215bba53273ff1384' https://siwaa.toulouse.inrae.fr/api/histories 

  # All histories owned by the user => saved into histories.json
  curl --output histories.json  --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:9e38ceeedc3c14a215bba53273ff1384' https://siwaa.toulouse.inrae.fr/api/histories 

  # Following functions use histories.json file !!!

  function get_history_id {
    history_name="$1"
    history_id=$(jq --arg NAME "$history_name" \
                 '.[] | select(.name == $NAME) | .id' histories.json)
    echo $history_id
  }

  function get_history_name {
    history_id="$1"
    history_name=$(jq --arg ID "$history_id" \
                 '.[] | select(.id == $ID) | .name' histories.json)
    echo $history_name
  }

  function get_history {
    history_id="$1"
    history=$(jq --arg ID "$history_id" \
              '.[] | select(.id == $ID)' histories.json)
    echo $history
  }

  # examples
  get_history_id "imported: sharedHistory2TestListDownload"
  get_history_name "a7b3549c61eb1e5b"
  get_history "a7b3549c61eb1e5b"

  # list of names 
  cat histories.json | jq '.[].name'

  # list of ids
  cat histories.json | jq '.[].id'

  # list : name id name id name ...
  cat histories.json | jq '.[] | .name, .id'

  ```

# A history content

  ```sh

  function get_history_content {
      history_id=$1
      curl --header "Content-Type: application/x-www-form-urlencoded" --header 'x-api-key:9e38ceeedc3c14a215bba53273ff1384' https://siwaa.toulouse.inrae.fr/api/histories/${history_id}/contents
  }

  # example

  history_id="fffb62a146375ea6"

  get_history_content $history_id 

  # Filter history content data with type:"collection" collection_type:"list"
  get_history_content $history_id | jq '.[] | select(.type == "collection") | select(.collection_type == "list")'

  ```

  Get id value of a history data, from its hid value :

  ```sh

  # For a data of a history :
  # - input :
  #   - history_id
  #   - data_hid : hid of the data into the history
  # - output :
  #   - data_id : id of the data into the history
  function get_data_id {
      history_id=$1
      data_hid=$2
      history_content=$(get_history_content $history_id)
      # note : data_hid required as int value $((data_hid))
      data=$(echo $history_content | jq --argjson HID $((data_hid)) \
                                        '.[] | select(.hid == $HID)')
      data_id=$(echo $data | jq -r '.id')
      echo $data_id
  }

  ```

