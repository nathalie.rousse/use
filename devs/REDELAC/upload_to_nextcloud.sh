#!/bin/bash

# REDELAC

# NextCloud

###############################################################################
#                             Functions
###############################################################################

function help {
   echo ""
   echo "Usage: $0 --help"
   echo "Usage: $0 -C <filepath> -D <url> -u <name> -p <password> -F <filepath> -R <filepath>"
   echo ""
   echo "Desc: 'upload_to_nextcloud' uploads a local file to NextCloud."
   echo ""
   echo "Arguments:"
   echo "  -C Configuration file path (.json)"
   echo "  -D NextCloud server url (Destination)"
   echo "  -u LDAP username"
   echo "  -p NextCloud Application password"
   echo "  -F Local file path to be uploaded (mandatory)"
   echo "  -R Remote uploaded file path destination (mandatory)"
   echo ""
   echo "Configuration:"
   echo "- Values required for NextCloud server url and Application password, LDAP username"
   echo "- Values taken from (by decreasing priority):"
   echo "    - -D and -u and -p values"
   echo "    - -C configuration file"
   echo "    - Default configuration file (\"config.json\")"
   echo ""
   echo "Auth: -u and -p values"
   echo ""
   if [ "$1" ]; then exit 1; else exit 0; fi
}

# -----------------------------------------------------------------------------
# NextCloud API

function upload_to_nextcloud {
    local_file_path=$1
    remote_file_path=$2
    user=${LDAPNAME}:${APPPASSWORD}
    curl --user "$user" --upload-file $local_file_path --request "PUT" --url "${NEXTCLOUD_SERVER}/remote.php/webdav/${remote_file_path}"
}

###############################################################################
#                             MAIN
###############################################################################

if [[ "$1" == "--help" ]]; then help; fi

while getopts C:D:u:p:F:R: flag
do
    case "${flag}" in
        C) CONFIG_PATH=${OPTARG};;
        D) NEXTCLOUD_SERVER=${OPTARG};;
        u) LDAPNAME=${OPTARG};;
        p) APPPASSWORD=${OPTARG};;
        F) local_file_path=${OPTARG};;
        R) remote_file_path=${OPTARG};;
        ?) help "errorcase";;
    esac
done

# -----------------------------------------------------------------------------
# Initialisation

echo "Initialisation..."

# NEXTCLOUD_SERVER LDAPNAME APPPASSWORD CONFIG_PATH
# Default file configuration "config.json"
if [ -z "$NEXTCLOUD_SERVER" ] || [ -z "$LDAPNAME" ] || [ -z "$APPPASSWORD" ]
then
    if [ -z "$CONFIG_PATH" ]; then CONFIG_PATH="config.json"; fi
    if [ ! -f "$CONFIG_PATH" ]
    then
        echo "Configuration file '$CONFIG_PATH' does not exist"
        exit 1
    fi
    CONFIG=$(cat $CONFIG_PATH)
    NEXTCLOUD=$(echo $CONFIG | jq '.nextcloud')
    if [ -z "$NEXTCLOUD_SERVER" ]
    then
        NEXTCLOUD_SERVER=$(echo $NEXTCLOUD | jq -r '.NEXTCLOUD_SERVER')
    fi
    if [ -z "$LDAPNAME" ]
    then
        LDAPNAME=$(echo $NEXTCLOUD | jq -r '.LDAPNAME')
    fi
    if [ -z "$APPPASSWORD" ]
    then
        APPPASSWORD=$(echo $NEXTCLOUD | jq -r '.APPPASSWORD')
    fi
fi
echo "=> NEXTCLOUD_SERVER : $NEXTCLOUD_SERVER"
echo "=> LDAPNAME :         $LDAPNAME"
echo "(& APPPASSWORD)"

# local_file_path
if [ -z "$local_file_path" ]
then
    echo "Path of local file to be uploaded required (-F option)"
    exit 1
fi
if [ ! -f "$local_file_path" ]
then
    echo "Local file path $local_file_path does not exist"
    exit 1
fi
echo "=> local_file_path :  $local_file_path"

# remote_file_path
if [ -z "$remote_file_path" ]
then
    echo "Remote uploaded file path destination required (-R option)"
    exit 1
fi

#remote_file_folder="$(dirname "${remote_file_path}")"
#remote_file_name="$(basename "${remote_file_path}")"

echo "=> remote_file_path : $remote_file_path"

# -----------------------------------------------------------------------------

echo "Upload file..."

upload_to_nextcloud $local_file_path $remote_file_path

# -----------------------------------------------------------------------------

