#!/bin/bash

# REDELAC

# Galaxy

###############################################################################
#                             Functions
###############################################################################

function help {
   echo ""
   echo "Usage: $0 --help"
   echo "Usage: $0 -C <filepath> -S <url> -k <key> -O <dirpath> -P \"<history id>:<Collection hid> ... <history id>:<Collection hid>\""
   echo ""
   echo "Desc: 'download_from_galaxy' downloads from Galaxy datasets (.zip files) of some collections of some histories, then gathered into a local 'analyse_simulations_STICS.zip' file."
   echo ""
   echo "Details : Datasets have some common points such as folders names ('analyse_simulations_STICS'...) and structure (cf https://siwaa.toulouse.inrae.fr/u/d37859d45b5e4fd4b53aa78bac1abb26/h/sharedhistory2testlistdownload, https://forgemia.inra.fr/redelac/redelac-toolbox/-/issues/65)"
   echo ""
   echo "Arguments:"
   echo "  -C Configuration file path (.json)"
   echo "  -S Galaxy server url (Source)"
   echo "  -k Galaxy API key"
   echo "  -O Output folder path (optional)"
   echo "  -P List of <history id>:<Collection hid> (mandatory)"
   echo ""
   echo "Configuration:"
   echo "- Values required for Galaxy server url and API key"
   echo "- Values taken from (by decreasing priority):"
   echo "    - -S and -k values"
   echo "    - -C configuration file"
   echo "    - Default configuration file (\"config.json\")"
   echo ""
   echo "Auth: API key from Galaxy (-k value)"
   echo ""
   if [ "$1" ]; then exit 1; else exit 0; fi
}

# -----------------------------------------------------------------------------
# Galaxy API

function get_url {
    url=$1
    ret=$(curl --header 'Content-Type: application/x-www-form-urlencoded' \
               --header 'x-api-key:'${API_KEY} ${url})
    echo $ret
}

# Histories
function get_histories {
    histories=$(get_url ${GALAXY_SERVER}/api/histories)
    echo $histories
}

function get_history_content {
    history_id=$1
    history_content=$(get_url ${GALAXY_SERVER}/api/histories/${history_id}/contents)
    echo $history_content
}

# Identify collections of a history
# Input : history_id
# Output : list of history_id:collection_hid
# Note : history collections (la 3e data "Job" ?)
function identify_collections {
    history_id=$1
    history_content=$(get_history_content $history_id)
    # filter history content data being collections
    history_collections=$(echo $history_content | \
        jq '.[] | select(.type == "collection") | select(.collection_type == "list") | select(.job_source_type != "Job")')
    for hid in $(echo $history_collections | jq -r '.hid'); do
      identity=$(echo $history_id)":"$(echo $hid)
      echo $identity
    done
}

# -----------------------------------------------------------------------------
# param history_id:collection_hid

function param_history_id {
    param=$1
    history_id=$(echo "$param" | cut -d ':' -f 1)
    echo $history_id
}
function param_collection_hid {
    param=$1
    collection_hid=$(echo "$param" | cut -d ':' -f 2)
    echo $collection_hid
}

# -----------------------------------------------------------------------------
# Download datasets of some collections of some histories
# Input : list of history_id:collection_hid
# Output : list of downloaded files names
function download_collections_hdas {
    params=$*
    for param in $params; do
        history_id=$(param_history_id $param)
        collection_hid=$(param_collection_hid $param)
        history_content=$(get_history_content $history_id)
        collection=$(echo $history_content | jq \
                                           --argjson HID $((collection_hid)) \
                                           '.[] | select(.hid == $HID)')
        collection_contents_url=$(echo $collection | jq -r '.contents_url')
        collection_content=$(curl --header '${CONTENT_TYPE}' \
                                  --header 'x-api-key:'${API_KEY} \
                                  ${GALAXY_SERVER}${collection_contents_url})
        # each data of collection content
        for hda_id in $(echo $collection_content | jq -r '.[].object.id'); do
            hda_download_url=${GALAXY_SERVER}/api/histories/${history_id}/contents/${hda_id}/display
            filename="hda_${history_id}_${collection_hid}_${hda_id}.zip"
            filepath="${OUTPATH}/${filename}"
            wget -P $OUTPATH -O $filepath $hda_download_url
            echo $filename
        done
    done
}

###############################################################################
#                             MAIN
###############################################################################

if [[ "$1" == "--help" ]]; then help; fi

while getopts C:S:k:O:P: flag
do
    case "${flag}" in
        C) CONFIG_PATH=${OPTARG};;
        S) GALAXY_SERVER=${OPTARG};;
        k) API_KEY=${OPTARG};;
        O) OUTPATH=${OPTARG};;
        P) params=${OPTARG};;
        ?) help "errorcase";;
    esac
done

# -----------------------------------------------------------------------------
# Initialisation

echo "Initialisation..."

# GALAXY_SERVER API_KEY CONFIG_PATH
# Default file configuration "config.json"
if [ -z "$GALAXY_SERVER" ] || [ -z "$API_KEY" ] 
then
    if [ -z "$CONFIG_PATH" ]; then CONFIG_PATH="config.json"; fi
    if [ ! -f "$CONFIG_PATH" ]
    then
        echo "Configuration file '$CONFIG_PATH' does not exist"
        exit 1
    fi
    CONFIG=$(cat $CONFIG_PATH)
    GALAXY=$(echo $CONFIG | jq '.galaxy')
    if [ -z "$GALAXY_SERVER" ] 
    then
        GALAXY_SERVER=$(echo $GALAXY | jq -r '.GALAXY_SERVER')
    fi
    if [ -z "$API_KEY" ] 
    then
        API_KEY=$(echo $GALAXY | jq -r '.API_KEY')
    fi
fi
echo "=> GALAXY_SERVER : $GALAXY_SERVER"

# OUTPATH
if [ -z "$OUTPATH" ]; then OUTPATH="."; fi
if [ ! -d $OUTPATH ]
then
    echo "Output folder path '$OUTPATH' does not exist"
    exit 1
fi
echo "=> OUTPATH :       $OUTPATH"

# params
if [ -z "$params" ] 
then
    echo "No parameter such as <history id>:<Collection hid> given (-d option)"
    exit 1
fi

# -----------------------------------------------------------------------------
# Verify available params values history_id:collection_hid
echo "Verify available -P params values..."
histories=$(get_histories)
for param in $params; do
    history_id=$(param_history_id $param)
    collection_hid=$(param_collection_hid $param)

    if [[ ! $(echo $histories | jq -r '.[].id' | fgrep -w $history_id) ]]
    then
        echo "Parameter $param : unavailable history_id value $history_id"
        exit 1
    fi
    history_collections=$(identify_collections $history_id)
    if [[ ! $(echo $history_collections | fgrep -w $param) ]]
    then
        echo "Parameter $param : unavailable value"
        exit 1
    fi
done

echo "=> params  :       $params"

# -----------------------------------------------------------------------------

#params="fffb62a146375ea6:21 fffb62a146375ea6:11"

echo "Download files..."

# download
downloaded_filenames=$(download_collections_hdas $params)

echo "=> downloaded files under $OUTPATH:"
echo "$downloaded_filenames"

echo "Unzip downloaded files, then final zip..."

# unzip then zip
home=$PWD
cd $OUTPATH
for hda_zip_file in $(echo $downloaded_filenames); do
    unzip $hda_zip_file
done
zip -r analyse_simulations_STICS.zip analyse_simulations_STICS
cd $home

echo "=> final .zip file ${OUTPATH}/analyse_simulations_STICS.zip"

###############################################################################
