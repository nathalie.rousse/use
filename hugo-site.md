## Hugo Site

### Pour tester ce site en local

La doc pour installer Hugo : https://gohugo.io/getting-started/quick-start/

Soit :
```sh
brew install hugo
hugo version  #pour vérifier
```

Puis cloner le dépôt :warning: inclure les sous modules pour les thèmes :warning: :
```sh
git clone --recurse-submodules git@forgemia.inra.fr:nathalie.rousse/use.git
```

Lancer Hugo :
```sh
cd com
hugo server -D
```

