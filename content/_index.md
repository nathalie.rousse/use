---
title: "As Software User"
type: "page"
---

Project https://forgemia.inra.fr/nathalie.rousse/use contains 
some notes and devs as Software User.

See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/main/README.md) *(README.md of 'main' branch)*.

## Siwaa API

- The [siwaa-api branch](https://forgemia.inra.fr/nathalie.rousse/use/-/tree/siwaa-api) of [use project](https://forgemia.inra.fr/nathalie.rousse/use) provides **documentation (text, code) about accessing to Siwaa by REST API**.
- See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/siwaa-api/README.md) *(README.md of 'siwaa-api' branch)*.
- [Download](https://forgemia.inra.fr/nathalie.rousse/use/-/archive/siwaa-api/use-siwaa-api.zip).

## GAMA

- The [gama branch](https://forgemia.inra.fr/nathalie.rousse/use/-/tree/gama) of [use project](https://forgemia.inra.fr/nathalie.rousse/use) provides **use cases (text, code)** of **agent-based simulations** of the **[GAMA](https://gama-platform.org) platform**, deployed as **containers**, as **Galaxy tools**, using **Siwaa API**.
- See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/gama/README.md) *(README.md of 'gama' branch)*.
- [Download](https://forgemia.inra.fr/nathalie.rousse/use/-/archive/gama/use-gama.zip).
- See some presentations :
  '[Case of deployed Galaxy tools](https://view.genially.com/67361387d808f58a421f9e44/interactive-content-gama-desc-gen-run-tools)',
  *'[A case in development state](https://view.genially.com/6654303dac73ac00151ced16/interactive-content-gamatool)'*.

## A Dense Neural Network

- The [dnn branch](https://forgemia.inra.fr/nathalie.rousse/use/-/tree/dnn) of [use project](https://forgemia.inra.fr/nathalie.rousse/use) provides a **use case (text, code)** of **a Dense Neural Network (DNN)** for **wine quality prediction**, deployed as **containers**, as **Galaxy tools**, using **Siwaa API**.
- See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/dnn/README.md) *(README.md of 'dnn' branch)*.
- [Download](https://forgemia.inra.fr/nathalie.rousse/use/-/archive/dnn/use-siwaa-api.zip).

## Data INRAE data.inrae.fr

- The [dataverse branch](https://forgemia.inra.fr/nathalie.rousse/use/-/tree/dataverse) of [use project](https://forgemia.inra.fr/nathalie.rousse/use) provides information (text, code) about **how to get information from Data INRAE data.inrae.fr** (download files...).
- See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/dataverse/README.md) *(README.md of 'dataverse' branch)*.
- [Download](https://forgemia.inra.fr/nathalie.rousse/use/-/archive/dataverse/use-dataverse.zip).

## NextCloud INRAE https://nextcloud.inrae.fr

- The [nextcloud branch](https://forgemia.inra.fr/nathalie.rousse/use/-/tree/nextcloud) of [use project](https://forgemia.inra.fr/nathalie.rousse/use) provides information (text, code) about **how to upload file to NextCloud INRAE and how to download file from NextCloud INRAE**.
- See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/nextcloud/README.md) *(README.md of 'nextcloud' branch)*.
- [Download](https://forgemia.inra.fr/nathalie.rousse/use/-/archive/nextcloud/use-nextcloud.zip).

## An environment to run Jupyter Notebooks

- The [ipynb-env branch](https://forgemia.inra.fr/nathalie.rousse/use/-/tree/ipynb-env) of [use project](https://forgemia.inra.fr/nathalie.rousse/use) provides **a remote environment to run Jupyter Notebooks**.
- See [README.md](https://forgemia.inra.fr/nathalie.rousse/use/-/blob/ipynb-env/README.md) *(README.md of 'ipynb-env' branch)*.

